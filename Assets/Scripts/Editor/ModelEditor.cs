﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Boo.Lang;

[CustomEditor(typeof(Model))]
public class LevelModelEditorScriptEditor : Editor
{
    protected List<Block> GetPrefabList()
    {
        string[] assetsPaths = AssetDatabase.GetAllAssetPaths();
        List<Block> _BlockPrefabs = new List<Block>();
        foreach (string assetPath in assetsPaths)
        {
            if (assetPath.Contains("Assets/Assets/Prefabs/Blocks/"))
            {
                _BlockPrefabs.Add(AssetDatabase.LoadAssetAtPath<Block>(assetPath));
            }
        }
        return _BlockPrefabs;
    }

    protected void InstantiatePrefab(Model _Target, E_ItemType PrefabType)
    {
        Block Prefab;
        List<Block> prefabList = GetPrefabList();
        prefabList.Find(x => x.GetItemType() == PrefabType, out Prefab);
        _Target.EditorAddBlock(Prefab);
    }

    public override void OnInspectorGUI()
    {
        string[]    _Options = new string[] { "Add Block", "CUBE 1x1", "CUBE 2x1", "CUBE 1x2", "CUBE 2x2", "CUBTE T", "SPHERE 1x1", "SPHERE 2x2", "CYLINDER 1x1" };
        int         _OptionIdx = 0;

        DrawDefaultInspector();

        Model       _Target = (Model)target;

        if (GUILayout.Button("Refresh Blocks"))
        {
            _Target.EditorRefreshBlocks();
        }

        _OptionIdx = EditorGUILayout.Popup(_OptionIdx, _Options);
        switch (_Options[_OptionIdx])
        {
            case "CUBE 1x1":
                InstantiatePrefab(_Target, E_ItemType.CUBE1x1);
                break;
            case "CUBE 2x1":
                InstantiatePrefab(_Target, E_ItemType.CUBE2x1);
                break;
            case "CUBE 1x2":
                InstantiatePrefab(_Target, E_ItemType.CUBE1x2);
                break;
            case "CUBE 2x2":
                InstantiatePrefab(_Target, E_ItemType.CUBE2x2);
                break;
            case "CUBTE T":
                InstantiatePrefab(_Target, E_ItemType.CUBE_T);
                break;
            case "SPHERE 1x1":
                InstantiatePrefab(_Target, E_ItemType.SPHERE1x1);
                break;
            case "SPHERE 2x2":
                InstantiatePrefab(_Target, E_ItemType.SPHERE2x2);
                break;
            case "CYLINDER 1x1":
                InstantiatePrefab(_Target, E_ItemType.CYLINDER1x1);
                break;
            // "CUBTE T", "SPHERE 1x1", "SPHERE 2x2", "CYLINDER 1x1"
            default:
                break;
        }
        EditorUtility.SetDirty(target);

    }
}