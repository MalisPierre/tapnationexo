﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class AudioInstance : MonoBehaviour
{

    [SerializeField]
    protected float _BaseVolume;
    void Start()
    {

    }

    void Update()
    {

    }

    public void SetAudioFile(AudioClip Clip, bool IsLooping, bool DestroyAfterLoop, float NewVolume)
    {
        this.GetComponent<AudioSource>().clip = Clip;
        this.GetComponent<AudioSource>().loop = IsLooping;
        this.GetComponent<AudioSource>().Play();
        if (DestroyAfterLoop == true)
            StartCoroutine(waitForSound());
        this._BaseVolume = NewVolume;
        this.RefreshVolume();
    }

    public void RefreshVolume()
    {
        this.SetVolume(this._BaseVolume * AudioManager.Instance.getVolume());
    }

    IEnumerator waitForSound()
    {
        while (this.GetComponent<AudioSource>().isPlaying)
        {
            yield return null;
        }
        Destroy(this.gameObject);
    }

    public void SetVolume(float NewVolume)
    {
        this.GetComponent<AudioSource>().volume = NewVolume;
    }

    public float GetVolume()
    {
        return this.GetComponent<AudioSource>().volume;
    }

    public string GetId()
    {
        return this.transform.name;
    }


}