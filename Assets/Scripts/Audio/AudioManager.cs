﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;

    public class AudioManager : MonoBehaviour
    {

        [SerializeField]
        AudioInstance   _Prefab = null;

    [SerializeField]
    float               _Volume = 0.5f;

        public static AudioManager Instance { get; private set; }


        void Awake()
        {

            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
            }
            Instance = this;
            DontDestroyOnLoad(transform.gameObject);

        }

        void Start()
        {

        }


        public AudioInstance AddAudioInstance(string AudioId, AudioClip Clip, float NewVolume, bool IsLooping, bool DestroyAfterLoop)
        {
            GameObject Instance = Instantiate(
                this._Prefab.gameObject,
                new Vector3(0, 0, 0),
                Quaternion.identity) as GameObject;
            Instance.transform.parent = this.transform;
            Instance.transform.name = AudioId;
            Instance.transform.position = Vector3.zero;

            Instance.GetComponent<AudioInstance>().SetAudioFile(Clip, IsLooping, DestroyAfterLoop, NewVolume);

            return Instance.GetComponent<AudioInstance>();
        }

        public void RemoveAudio(string AudioId)
        {
            Destroy(this.transform.Find(AudioId).gameObject);
        }

        public AudioInstance FindAudio(string AudioId)
        {
            return this.transform.Find(AudioId).gameObject.GetComponent<AudioInstance>();
        }


        public AudioInstance GetAudio(int Idx)
        {
            return this.transform.GetChild(Idx).gameObject.GetComponent<AudioInstance>();
        }

        public int GetAudioCount()
        {
            return this.transform.childCount;
        }

        public bool CheckAudioExist(string AudioId)
        {
            if (this.transform.Find(AudioId) != null)
                return true;
            else
                return false;
        }

    public void SetVolume(float newVolume)
    {
        this._Volume = newVolume;
        for(int i = 0; i < this.GetAudioCount(); i++)
        {
            this.GetAudio(i).RefreshVolume();
        }
    }

    public float getVolume()
    {
        return this._Volume;
    }

    }