﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Script : MonoBehaviour
{
    [SerializeField]
    private Slider _GravitySlider = null;

    [SerializeField]
    private Slider _SpawnSlider = null;

    [SerializeField]
    private Slider _AudioSlider = null;

    [SerializeField]
    private ItemGenerator _Generator = null;

    [SerializeField]
    private GameObject _MainMenu = null;

    [SerializeField]
    private GameObject _WinPanel = null;

    [SerializeField]
    private AudioClip _NormalClick = null;
    [SerializeField]
    private AudioClip _ExitClick = null;
    [SerializeField]
    private AudioClip _NewGameClick = null;
    [SerializeField]
    private AudioClip _WinClick = null;

    [SerializeField]
    private AudioClip _MainMusic = null;
    [SerializeField]
    private AudioClip _WinMusic = null;
    [SerializeField]
    private AudioClip _PlayMusic = null;

    [SerializeField]
    private List<Model> _Models = new List<Model>();

    void Start()
    {
        this.OnMainMenu();
        AudioManager.Instance.AddAudioInstance("BackgroundMusic", this._MainMusic, 0.5f, true, false);
    }

    void Update()
    {
        
    }

    public void OnGravitySpeed()
    {
        this.PlayNormalClick("GravityClick");
        this._Generator.SetGravitySpeed(this._GravitySlider.value);
    }

    public void OnSpawnSpeed()
    {
        this.PlayNormalClick("SpawnClick");
        this._Generator.SetSpawnRate(this._SpawnSlider.value);
    }

    public void OnAudioVolume()
    {
        Debug.Log("Setting Audio To (" + this._AudioSlider.value + ")");
        this.PlayNormalClick("AudioClick");
        AudioManager.Instance.SetVolume(this._AudioSlider.value / 100.0f);
    }

    public void OnPlay()
    {
        this.PlayNewGameClick("New Game");
        this._MainMenu.SetActive(false);
        this._WinPanel.SetActive(false);
        int ModelIdx = Random.Range(0, this._Models.Count);
        Model NewModel = (Model)Instantiate(this._Models[ModelIdx]);
        NewModel.SetUI(this);
        this._Generator.Play();
    }

    public void OnWin()
    {
        this.PlayWinClick("On Win");
        this._Generator.Stop();
        this._MainMenu.SetActive(false);
        this._WinPanel.SetActive(true);
    }

    public void OnExit()
    {
        this.PlayExitClick("On Exit");
        Application.Quit();
    }

    public void OnMainMenu()
    {
        this.PlayExitClick("On Return");
        this._Generator.Stop();
        this._MainMenu.SetActive(true);
        this._WinPanel.SetActive(false);
    }

    public void Onretry()
    {

    }


    public void PlayNormalClick(string id)
    {
        AudioManager.Instance.AddAudioInstance(id, this._NormalClick, 1.0f, false, true);
    }

    public void PlayNewGameClick(string id)
    {
        AudioManager.Instance.AddAudioInstance(id, this._NewGameClick, 1.0f, false, true);
    }

    public void PlayWinClick(string id)
    {
        AudioManager.Instance.AddAudioInstance(id, this._WinClick, 1.0f, false, true);
    }

    public void PlayExitClick(string id)
    {
        AudioManager.Instance.AddAudioInstance(id, this._ExitClick, 1.0f, false, true);
    }

    public void PlayMenuMusic()
    {
        AudioManager.Instance.FindAudio("BackgroundMusic").SetAudioFile(this._MainMusic, true, false, 0.5f);
    }
    public void PlayWinMusic()
    {
        AudioManager.Instance.FindAudio("BackgroundMusic").SetAudioFile(this._WinMusic, true, false, 0.5f);
    }
    public void PlayGameMusic()
    {
        AudioManager.Instance.FindAudio("BackgroundMusic").SetAudioFile(this._PlayMusic, true, false, 0.5f);
    }

}
