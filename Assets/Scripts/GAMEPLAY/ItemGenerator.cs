﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemGenerator : MonoBehaviour
{
    [SerializeField]
    private List<GameObject>    _SpawnPosition = new List<GameObject>();

    [SerializeField]
    private List<Item>          _Prefabs = new List<Item>();

    [SerializeField]
    private bool                _CanSpawn = true;

    [SerializeField]
    private bool                _IsPlaying = false;

    [SerializeField]
    private float               _DelayBetweenSpawn = 4.0f;

    [SerializeField]
    private float               _GravitySpeed = 3.0f;
    void Start()
    {
        this._CanSpawn = true;
    }
    
    void Update()
    {
        if (this._IsPlaying == true)
        {
            if (this._CanSpawn == true)
            {
                int NextPrefabIndex = Random.Range(0, this._Prefabs.Count);

                StartCoroutine(WaitForNextSpawn(NextPrefabIndex));
            }
        }

    }

    IEnumerator WaitForNextSpawn(int NextPrefabIndex)
    {
        this._CanSpawn = false;
        int SpawnIdx = Random.Range(0, this._SpawnPosition.Count);
        Item NewInstance = (Item)Instantiate(this._Prefabs[NextPrefabIndex], this._SpawnPosition[SpawnIdx].transform.position, this._SpawnPosition[SpawnIdx].transform.rotation);
        NewInstance.GetComponent<Rigidbody>().drag = this._GravitySpeed;
        yield return new WaitForSeconds(this._DelayBetweenSpawn);
        this._CanSpawn = true;
    }

    public void SetSpawnRate(float NewVal)
    {
        this._DelayBetweenSpawn = NewVal;
    }

    public void SetGravitySpeed(float NewVal)
    {
        this._GravitySpeed = NewVal;
    }

    public void Play()
    {
        this._IsPlaying = true;
    }

    public void Stop()
    {
        this._IsPlaying = false;
    }

}
