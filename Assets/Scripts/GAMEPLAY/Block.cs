﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    [SerializeField]
    private bool _IsActivated;

    [SerializeField]
    private E_ItemType _Type = E_ItemType.CUBE1x1;

    [SerializeField]
    private List<Block> _Requirement = new List<Block>();
    
    void Start()
    {
        
    }
    
    void Update()
    {
        
    }

    public bool IsActivated()
    {
        return this._IsActivated;
    }

    public E_ItemType GetItemType()
    {
        return this._Type;
    }

    public void Activate(Item item)
    {
        for (int i = 0; i < this.transform.childCount; i++)
        {
            this.transform.GetChild(i).GetComponent<MeshRenderer>().sharedMaterial = item.transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial;
            this.transform.GetChild(i).GetComponent<Collider>().isTrigger = false;
        }
        
        Destroy(item.gameObject);
        this._IsActivated = true;
        
        this.transform.parent.gameObject.GetComponent<Model>().CheckIsFinished();
    }

    public bool HasFinishedReqirements()
    {
        foreach(Block Require in this._Requirement)
        {
            if (Require.IsActivated() == false)
                return false;
        }
        return true;
    }
}
