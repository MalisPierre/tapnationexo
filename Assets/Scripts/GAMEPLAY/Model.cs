﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Model : MonoBehaviour
{
    [SerializeField]
    protected List<Block>   _Blocks = new List<Block>();

    [SerializeField]
    protected UI_Script _UserInterface;

    public void AddBlock(Block block)
    {
        this._Blocks.Add(block);
    }

    void Start()
    {

    }

    void Update()
    {

    }

    public void CheckIsFinished()
    {
        this._UserInterface.PlayNewGameClick("Block Activated");
        foreach(Block block in this._Blocks)
        {
            if (block.IsActivated() == false)
                return;
        }
        //GAME END
        this._UserInterface.OnWin();
        Destroy(this.gameObject);
    }

    // EDITOR SCRIPT
    public void EditorRefreshBlocks()
    {
#if (UNITY_EDITOR)
        this._Blocks = new List<Block>();
        for(int i = 0; i < this.transform.childCount; i++)
        {
            this._Blocks.Add(this.transform.GetChild(i).GetComponent<Block>());
        }
#endif
    }

    // EDITOR SCRIPT
    public void EditorAddBlock(Block Prefab)
    {
#if (UNITY_EDITOR)
        Block Instance = (Block)PrefabUtility.InstantiatePrefab(Prefab, this.transform);
        Instance.transform.position = new Vector3(0, 5, 0);
        Instance.transform.name = Prefab.transform.name;
        EditorRefreshBlocks();
        return;
#endif
    }

    public void SetUI(UI_Script UIRef)
    {
        this._UserInterface = UIRef;
    }

}
