﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum E_ItemType
{
    CUBE1x1 = 0,
    CUBE2x1 = 1,
    CUBE1x2 = 2,
    CUBE2x2 = 3,
    CUBE_T = 4,
    SPHERE1x1 = 11,
    SPHERE2x2 = 12,
    CYLINDER1x1 = 21
}

public class Item : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    private E_ItemType _Type = E_ItemType.CUBE1x1;

    [SerializeField]
    private bool _MustDestroy = false;

    private Vector3 _ScreenPoint;
    private Vector3 _Offset;

    void Start()
    {
        
    }
    
    void Update()
    {
        
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Destroy(gameObject);
    }


    void OnMouseDown()
    {
        this._ScreenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        this._Offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, this._ScreenPoint.z));
    }

    void OnMouseDrag()
    {
        if (this.IsGrounded() == false)
        {
            Vector3 cursorPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, this._ScreenPoint.z);
            Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + this._Offset;
            transform.position = new Vector3(cursorPosition.x, this.transform.position.y, this.transform.position.z);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Limit")
        {
            // COLLISION WITH DESTROYER
            StartCoroutine(DestroyCountRoutine());
        }
    }

    public bool IsGrounded()
    {
        Debug.Log(Mathf.Abs(this.GetComponent<Rigidbody>().velocity.y));
        if (Mathf.Abs(this.GetComponent<Rigidbody>().velocity.y) <= 0.025f)
            return true;
        return false;
    }

    void OnTriggerStay(Collider other)
    {
        if (this._MustDestroy == true)
        {
            if (
                (this.IsGrounded()) ||
                (other.tag == "Limit")
                )
            Debug.Log("Destroying GO");
            Destroy(this.gameObject);
        }
        if (other.gameObject.tag == "Block")
        {
            Block block = other.gameObject.GetComponent<Block>();
            if (
                (Vector3.Distance(this.transform.position, block.transform.position) < 0.25f) &&
                (block.GetItemType() == this._Type) &&
                (!block.IsActivated()) &&
                (block.HasFinishedReqirements())
                )
            {
                // ACTIVATE BLOCK !
                block.Activate(this);
            }
        }
        if ((other.gameObject.tag == "Floor") || (other.gameObject.tag == "Item") || (other.gameObject.tag == "Block"))
        {
            if (this.IsGrounded())
            {
                //GROUNDED ON FLOOR/ITEM/BLOCK  OBJECT
                StartCoroutine(DestroyCountRoutine());
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {

    }

    IEnumerator DestroyCountRoutine()
    {
        yield return new WaitForSeconds(1.5f);
        this._MustDestroy = true;
    }

}
